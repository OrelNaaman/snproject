@extends('layouts.sidebar')

@section('title','Orders')

@section('content')

@if(Session::has('notallowed'))
<div class='alert alert-danger'>
    {{Session::get('notallowed')}}
</div>

@endif
         @if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
        @endif

    
    @if (Request::is('myOrders')) 
        <h1 title='My order' style="float: left;"><b>My Orders</b></h1> 
        <h1 title='Add a new order' style="float: right;"><b><a href="{{ url('orders/create') }}">+</a></b></h1> 
        <br></br>
        <br></br>
        <h6><a  href="{{action('OrdersController@index')}}">Show All Orders</a></h6>   
    @else
        <h1 title='All order' style="float: left;"><b>All Orders</b></h1>
        <h1 title='Add a new order' style="float: right;"><b><a href="{{ url('orders/create') }}">+</a></b></h1>
        <br></br>
        <br></br>
        <h6><a href="{{ url('myOrders') }}">Show My Orders</a></h6>

    @endif
  
    

    <!-- <div class="panel-heading">
@if (Request::is('myOrders')) 
<h3 class="panel-title" >My Orders</h3>
@endif
</div> -->
    <div class="table-responsive">
    <table class="table align-items-center table-flush" >
            <tr>
            <th>Id</th><th>Seller</th><th>Customer</th><th>Product</th><th>Engine Code</th><th>Address</th><th>Price</th><th>Total Price</th><th>Is Paid?</th><th>Delivery Date</th><th>Status Order</th><th>Edit</th><th>More Details</th>
              
            </tr>
        <!-- the table data -->
            @foreach($orders as $order)
                   
            <tr>
            <td> {{$order->id}}</td>
            <td>
                <div class="dropdown">
                    <!-- <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
                        @if(isset($order->user_id))
                          {{$order->owner->name}}  
                        @else
                          Assign owner
                        @endif
                    </button>
                  
                  </div> 
            </td>  
            <td>{{$order->customer}}</td>             
            <td> @if(isset($order->product_id))
                          {{$order->own->name}}  
                        @else
                          Assign own
                        @endif</td>
            <td> {{$order->model}}</td> 
            <td> {{$order->address}}</td>
            <td> {{$order->totalPrice}}</td>
            <td> {{$order->totalPrice * 1.17}}</td>
            <td> {{$order->isPaid}}
            <td> {{$order->deliveryDate}}</td>
            <td>
                <div class="dropdown">
                    @if (null != App\Status::next($order->status_id))    
                    <button class="dropdown-toggle" style="background:#99e6e6" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (isset($order->status_id))
                           {{$order->status->name}}
                        @else
                            Define status
                        @endif
                    </button>
                    @else 
                    {{$order->status->name}}
                    @endif
                                                   
                    @if (App\Status::next($order->status_id) != null )
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\Status::next($order->status_id) as $status)
                         <a class="dropdown-item" href="{{route('orders.changestatus', [$order->id,$status->id])}}">{{$status->name}}</a>
                        @endforeach                               
                    </div>
                    @endif
                </div>                            
            </td>
            <td><a class="badge badge-info " style="background:#99e6e6" href = "{{route('orders.edit',$order->id)}}">Edit</a></td>
            <td> <a class="badge badge-info " style="background:#99e6e6" href="{{ route('orders.show',$order->id) }}"> Click Here </a> </td>
           

        </tr>
            @endforeach
    </table>

    {{$orders->links()}}
@endsection
