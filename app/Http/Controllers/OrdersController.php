<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Order;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('owner','own')->where([['status_id','!=', '4'],['status_id','!=', '5']])->orderBy('deliveryDate')->paginate(5);
        $users = User::all();
        $statuses = Status::all();
        return view('Orders.index', compact('orders','users','statuses')); 
    }
    public function myOrders()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $orders =   Order::with('owner')->where([['user_id', $userId],['status_id','!=', '4'],['status_id','!=', '5']])->paginate(5);
        // $orders = $user->orders;
        $users = User::all();
        //statuses = Status::all();        
        return view('Orders.index', compact('orders','users'));
    }

    public function statusDone()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $orders =   Order::with('owner')->where('status_id', '4')->paginate(5);
        // $orders = $user->orders;
        $users = User::all();
        $statuses = Status::all();        
        return view('Orders.index', compact('orders','users','statuses'));
    }

    public function statusCancel()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $orders =   Order::with('owner')->where('status_id', '5')->paginate(5);
        // $orders = $user->orders;
        $users = User::all();
        $statuses = Status::all();        
        return view('Orders.index', compact('orders','users','statuses'));
    }

    public function changeUser($cid, $uid = null){
        Gate::authorize('assign-user');
        $order = Order::findOrFail($cid);
        $order->user_id = $uid;
        $order->save(); 
        return back();

    }

    public function changeStatus($cid, $sid)
    {
        $order = Order::findOrFail($cid);
        if(Gate::allows('change-status', $order))
        {
            $from = $order->status->id;
            if(!Status::allowed($from,$sid)) return redirect('orders');        
            $order->status_id = $sid;
            $order->save();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the status of the user becuase you are not the owner of the user');
        }
        return back();
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $orders = Order::all();
        // $product = Product::all();
        $users = User::all();
        $products = Product::all();
        $id=Auth::id();
        $user_name = DB::table('users')->where('id', $id)->first();
        // return view('orders.create', compact('orders','users','products' ));
        return view('Orders.create', compact('orders','users','user_name','products' ));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order();
        
        $order->model = $request->model;
        $order->address = $request->address;
        $order->totalPrice = $request->totalPrice;
        $order->isPaid = $request->isPaid;
        $order->deliveryDate = $request->deliveryDate;
        $order->telephone = $request->telephone;
        $order->user_id = $request->user_selected;
        $order->product_id = $request->product_selected;
        $order->customer = $request->customer;
        $order->remarks = $request->remarks;
        $order->save();
        return redirect('orders');
        // return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orders = Order::findOrFail($id);
        return view('Orders.show', ['orders' => $orders]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $order = Order::findOrFail($id);
        $users = User::all();
        $products = Product::all();
        return view('Orders.edit', compact('order', 'products', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->model = $request->model;
        $order->address = $request->address;
        $order->totalPrice = $request->totalPrice;
        $order->isPaid = $request->isPaid;
        $order->telephone = $request->telephone;
        $order->customer = $request->customer;
        $order->remarks = $request->remarks;
        $order->deliveryDate = $request->deliveryDate;

        $order->update($request->all());// update all data
        return redirect()->route('orders.index')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();
        return redirect('Orders')->with('message','The Order has been Removed');
    }
}
