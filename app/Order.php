<?php

namespace App;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'model','address','totalPrice','isPaid','telephone','deliveryDate','user_id','status_id','product_id'
    ];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function own(){
        return $this->belongsTo('App\Product','product_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    } 
}
