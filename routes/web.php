<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
// use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', function () {
    return view('welcome');
});




Route::get('orders/delete/{id}','OrdersController@destroy')->name('order.delete');
Route::get('myOrders', 'OrdersController@myOrders')->name('orders.myOrders')->middleware('auth');
Route::get('statusdone', 'OrdersController@statusDone')->name('orders.statusDone')->middleware('auth');
Route::get('statuscancel', 'OrdersController@statusCancel')->name('orders.statusCancel')->middleware('auth');
// Route::get('backPage', 'OrdersController@backPage')->middleware('auth');
Route::resource('users', 'UsersController')->middleware('auth');
Route::get('orders/edit/{id}','OrdersController@edit')->name('order.edit');

Route::get('orders', 'OrdersController@index')->name('orders.index')->middleware('auth');

// Route::get('/home', [HomeController::class, 'index'])->name('home');
// Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
Route::get('/home',[
    'uses'=>'HomeController@index',
    'as'=>'home',
    
            ]);

 Route::resource('orders', 'OrdersController')->middleware('auth');

 Route::get('orders/changestatus/{cid}/{sid}', 'OrdersController@changeStatus')->name('orders.changestatus')->middleware('auth');

//  Route::post('/backPage', function () {
//     // Validate the request...

//     return back()->withInput();
// });


            